#include "Trainer.h"
#include "Pokemon.h"
#include "Pokedex.h"
#include "GameManager.h"
#include <iostream>

Introduction::Introduction() {
	Intro = "Oak: Welcome to the world of pokemon! Before you start your journey, Can I ask your name?";
};

void Introduction::printStats() {
	cout << R"(  
                                  ,'\
    _.----.        ____         ,'  _\   ___    ___     ____
_,-'       `.     |    |  /`.   \,-'    |   \  /   |   |    \  |`.
\      __    \    '-.  | /   `.  ___    |    \/    |   '-.   \ |  |
 \.    \ \   |  __  |  |/    ,','_  `.  |          | __  |    \|  |
   \    \/   /,' _`.|      ,' / / / /   |          ,' _`.|     |  |
    \     ,-'/  /   \    ,'   | \/ / ,`.|         /  /   \  |     |
     \    \ |   \_/  |   `-.  \    `'  /|  |    ||   \_/  | |\    |
      \    \ \      /       `-.`.___,-' |  |\  /| \      /  | |   |
       \    \ `.__,'|  |`-._    `|      |__| \/ |  `.__,'|  | |   |
        \_.-'       |__|    `-._ |              '-.|     '-.| |   |
                                `'                            '-._|
                 ____                    _     _          
                / ___|  __ _ _ __  _ __ | |__ (_)_ __ ___ 
                \___ \ / _` | '_ \| '_ \| '_ \| | '__/ _ \
                 ___) | (_| | |_) | |_) | | | | | | |  __/
                |____/ \__,_| .__/| .__/|_| |_|_|_|  \___|
                            |_|   |_|      
     )"<< endl;
	system("pause");
	system("cls");
	cout << Intro << endl;
}

void GameManager::userInput(Trainer*player ) {
	cout << "Please Enter your name: " << endl;
	cin >> name;
	system("pause");
	system("cls");
	Pokemon*poke = new Pokemon;
	poke->Starter(poke);
    Action(player);
}
void GameManager::Action(Trainer*player) {
	while(true) {
		cout << "What would you like to do? \n"
			<< "[1] - Move [2] - Pokemons [3] - Pokemon Center [4] - Pokedex" << endl;
		int decision;
		cin >> decision;
		system("cls");
		switch (decision) {
		case 1:
			player->position(player);
			break;
		case 2:
			cout << "You only have your starter pokemon for now" << endl;
			break;
		case 3:
			cout << "Your pokemon have been fully healed! come again!" << endl;
			break;
		case 4:
			Pokedex*entry = new Pokedex;
			entry->POKEDEX(entry);
			break;

		}
		system("Pause");
		system("cls");
	}
}

