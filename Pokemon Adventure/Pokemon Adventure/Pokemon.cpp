#include "Pokemon.h"
#include "Trainer.h"
#include "GameManager.h"
#include <iostream>
#include <vector>
#include <string>
using namespace std;
Pokemon::Pokemon() {
	name = "";
	type = "";
	baseHp = 0;
	hp = 0;
	level = 0;
	baseDmg = 0;
	exp = 0;
	expToNextLevel = 0;

}
void Pokemon::Starter(Pokemon*player) {
	cout << "I have three (3) Pokemons here.\n"
		 << "You can have one! Go ahead and choose!" << endl;
	cout <<" [1] - Torchic Fire  Type Lvl 5  \n [2] - Treecko Grass Type Lvl 5 \n [3] - Mudkip  Water Type Lvl 5" << endl;
	int choose;
	cout << " Input you chouce: ";
	cin >> choose;
	switch (choose) {
	case 1:
		cout << "You have chosen " << player->Torchic<<endl;
		break;
	case 2:
		cout << "You have chosen " <<player->Treecko<<endl;
		break;
	case 3:
		cout << "You have chosen "<< player->Mudkip<<endl;
		break;
	default:
		cout << "Please choose a valid option";
	}
	system("Pause");
	system("cls");
}




void Pokemon::printStats(string name, string type, int baseHp, int hp, int level, int baseDmg, int exp, int expToNextLevel)
{
	
	cout << "================ Stats=========== " << endl;
	cout << "Pokemon: " << name << endl;
	cout << "Type: " << type << endl;
	cout << "Base Health: " << baseHp << endl;
	cout << "Health: " << hp << endl;
	cout << "Level: " << level << endl;
	cout << "Base damage: " << baseDmg << endl;
	cout << "Exp.Points: " << exp <<endl;
	cout << "Next Lv: " << expToNextLevel <<endl;
}


