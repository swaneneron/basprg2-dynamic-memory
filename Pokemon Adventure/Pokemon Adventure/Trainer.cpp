#include "Trainer.h"
#include "GameManager.h"
#include "Pokedex.h"
#include <iostream>
#include <time.h>
using namespace std;
Trainer::Trainer(){
	name = "";
}

void Trainer::userInput(Trainer*player){
	cout << "Please Enter your name: " << endl;
	cin >> name;
	system("pause");
	system("cls");
}

void Trainer::position(Trainer*player) {
	cout << "Where would you like to move to?\n"
		<< "[W] - UP    [S] - Down    [A] - Left    [D] - Right" << endl;
	char choice;
	cin >> choice;
	switch (choice) {
	case 'W':
		player->y += 1;
		break;
	case 'S':
		player->y -= 1;
		break;
	case 'A':
		player->x -= 1;
		break;
	case 'D':
		player->x += 1;
		break;
	default:
		cout << "Please choose a valid option";
	}
	system("Pause");

	cout << "You are now at position " << "(" << x << "," << y << ")" << endl;

	if (x <= 4 && y <= 4) {
		cout << "You are now at Little Root town" << endl;
	}
	if (x == 5 && y ==5) {
		cout << "You are now at Root 101" << endl;
	}
	if (x == 6 && y == 6) {
		cout << "You are at a Wild Area, Be careful!" << endl;
		srand(time(NULL));
		cout << "You lurk in the area....A wild pokemon appears!" << endl;
		cout << "Would you like to battle? y/n" << endl;
		char ok;
		int enemyHp = 100;
		int pokemon = 100;
		int&enemRef = enemyHp;
		int&plaRef = pokemon;
		int enemAtk = 10;
		int plaAtk = 10;
		int&enemATKRef = enemAtk;
		int&plaATKref = plaAtk;
		cin >> ok;
		switch (ok)
		{
		case 'y':
			while (enemyHp >0 || pokemon> 0)
			{
				cout << "You are in battle! Continue or attempt to catch pokemon? Battle- 1 / Catch -2/ Run-3" << endl;
				cout << "==============================" << endl;
				cout << "Your Pokemon Current HP:" << plaRef << endl;
				cout << "Your Pokemon power:" << plaAtk << endl;
				cout << "______________________________" << endl;
				cout << "Enemy Current HP:" << enemRef << endl;
				cout << "Enemy attack power:" << enemAtk << endl;
				cout << "==============================" << endl;
				int ans;
				cin >> ans;
				if (ans == 1)
				{
					
					int atkRate= rand() % 101;
					if (atkRate <= 80) {
						enemRef = enemRef - plaAtk;
						cout << "Your pokemon attacks!" << endl;
						cout << "Your pokemon does " << plaAtk << " damage to the opponent!" << endl;
						plaRef = plaRef - enemAtk;
						cout << "Wild Pokemon does an attack!" << endl;
						cout << "Wild Pokemon does  " << plaAtk << " damage to the opponent!" << endl;
						system("pause");
					}
					if (atkRate >= 81){
						cout << "Your pokemon misses!" << endl;
						plaRef = plaRef - enemAtk;
						cout << "Wild Pokemon does an attack!" << endl;
						cout << "Wild Pokemon does  " << plaAtk << " damage to the opponent!" << endl;
						system("pause");
					}
				}
				if (ans == 2) {
					cout << "You threw a pokeball!" << endl;
					int catchRate = rand() % 101;
					if (catchRate <= 70) {
						cout << "The Wild pokemon managed to break free!" << endl;
					}
					if (catchRate >= 71) {
						cout << "The pokeball shakes....ping...ping..ping!! You've caught the pokemon!" << endl;
						break;
					}
				}
				if (ans == 3) {
					cout << "You runaway safely" << endl;
					break;
				}
			}
			break;
		case 'n':
			cout << "You choose to runaway!" << endl;
		}
		
	}
	if (x == 7 && y == 7) {
	    char z;
		cout << "You are now at the Oldale Town Pokemon Center!" << endl;
		cout << "Heal your pokemon? press y(yes)/n(no" << endl;
		cin >> z;
		if (z == 'y') {
			cout << "Your pokemon have been fully restored!" << endl;
		  }
		else if (z == 'x') {
			cout << "Thank you and come again!" << endl;
		}

	}
	system("pause");
	system("cls");
}

