
#pragma once
#include <string>
using namespace std;
class Pokedex {

public:
	Pokedex(); //constructor = use to initialize variables
	int pokeId;
	void POKEDEX(Pokedex*player);
	void printStats(int id, string name, string title, string type, int height, int weight);
	
private:
	int id;
	string name;
	string title;
	string type;
	int height;
	int weight;

};