
#pragma once
#include <string>
using namespace std;
class Pokemon {
public:
	Pokemon(); //constructor = use to initialize variables

	int choose;

	string name;
	string type;
	int baseHp;
	int hp;
	int level;
	int baseDmg;
	int exp;
	int expToNextLevel;

	string Torchic = "Torchic";
	string Treecko = "Treecko";
	string Mudkip = "Mudkip";

	void Starter(Pokemon*player);
	void printStats(string name, string type, int baseHp, int hp, int level, int baseDmg, int exp, int expToNextLevel);

	

};


