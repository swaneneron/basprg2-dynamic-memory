#include "Pokedex.h"
#include "Pokemon.h"
#include "Trainer.h"
#include "GameManager.h"
#include <iostream>
void Pokedex::POKEDEX(Pokedex*player) {
	cout << "Where would you like to move to?\n"
		<< "Input pokemon id [1-15]: " << endl;
	int pokeId;
	cin >> pokeId;
	switch (pokeId) {
	case 1:
		cout << "ID  : 1" << endl;
		cout << "Name: Treecko " << endl;
		cout << "Title: Wood Gecko Pokemon" << endl;
		cout << "Type: Grass" << endl;
		cout << "Height: 1'08" << endl;
		cout << "Weight: 11.0lbs" << endl;
		break;
	case 2:
		cout << "ID  : 2" << endl;
		cout << "Name: Torchic " << endl;
		cout << "Title: Chick Pokemon" << endl;
		cout << "Type: Fire" << endl;
		cout << "Height: 1'04" << endl;
		cout << "Weight: 5.5lbs" << endl;
		break;
	case 3:
		cout << "ID  : 3" << endl;
		cout << "Name: Mudkip " << endl;
		cout << "Title: Mud Fish Pokemon" << endl;
		cout << "Type: Water" << endl;
		cout << "Height: 1'04" << endl;
		cout << "Weight: 16.8lbs" << endl;
		break;
	case 4:
		cout << "ID  : 4" << endl;
		cout << "Name: Combusken " << endl;
		cout << "Title: Young Fowl Pokemon" << endl;
		cout << "Type: Fighting/Fire" << endl;
		cout << "Height: 2'11" << endl;
		cout << "Weight: 43.0lbs" << endl;
		break;
	case 5:
		cout << "ID  : 5" << endl;
		cout << "Name: Blaziken " << endl;
		cout << "Title: Blaze Pokemon" << endl;
		cout << "Type: Fighting/Fire" << endl;
		cout << "Height: 6'03" << endl;
		cout << "Weight: 114.6lbs" << endl;
		break;
	case 6:
		cout << "ID  : 6" << endl;
		cout << "Name: Poochyena" << endl;
		cout << "Title: Bite Pokemon" << endl;
		cout << "Type: Dark" << endl;
		cout << "Height: 1'08" << endl;
		cout << "Weight: 30.0lbs" << endl;
		break;
	case 7:
		cout << "ID  : 7" << endl;
		cout << "Name: Mightyena " << endl;
		cout << "Title: Bite Pokemon" << endl;
		cout << "Type: Dark" << endl;
		cout << "Height: 3'03" << endl;
		cout << "Weight: 81.6lbs" << endl;
		break;
	case 8:
		cout << "ID  : 8" << endl;
		cout << "Name: Ralts " << endl;
		cout << "Title: Feeling Pokemon" << endl;
		cout << "Type: Psychic" << endl;
		cout << "Height: 1'04" << endl;
		cout << "Weight: 14.6lbs" << endl;
		break;
	case 9:
		cout << "ID  : 9" << endl;
		cout << "Name: Kirlia " << endl;
		cout << "Title: Emotion Pokemon" << endl;
		cout << "Type: Psychic" << endl;
		cout << "Height: 2'07" << endl;
		cout << "Weight: 44.5lbs" << endl;
		break;
	case 10:
		cout << "ID  : 10" << endl;
		cout << "Name: Gardevoir " << endl;
		cout << "Title: Embrace Pokemon" << endl;
		cout << "Type: Grass" << endl;
		cout << "Height: 1'08" << endl;
		cout << "Weight: 11.0lbs" << endl;
		break;
	case 11:
		cout << "ID  : 11" << endl;
		cout << "Name: Eevee " << endl;
		cout << "Title: Evolution Pokemon" << endl;
		cout << "Type: Normal" << endl;
		cout << "Height: 1'00" << endl;
		cout << "Weight: 14.3lbs" << endl;
		break;
	case 12:
		cout << "ID  : 12" << endl;
		cout << "Name: Vaporeon " << endl;
		cout << "Title: Bubble Jet Pokemon" << endl;
		cout << "Type: Water" << endl;
		cout << "Height: 3'03" << endl;
		cout << "Weight: 63.9lbs" << endl;
		break;
	case 13:
		cout << "ID  : 13" << endl;
		cout << "Name: Jolteon " << endl;
		cout << "Title: Lightning Pokemon" << endl;
		cout << "Type: Electricity" << endl;
		cout << "Height: 2'07" << endl;
		cout << "Weight: 54.0lbs" << endl;
		break;
	case 14:
		cout << "ID  : 14" << endl;
		cout << "Name: Flareon " << endl;
		cout << "Title: Flame Pokemon" << endl;
		cout << "Type: Fire" << endl;
		cout << "Height: 2'11" << endl;
		cout << "Weight: 55.1lbs" << endl;
		break;
	case 15:
		cout << "ID  : 15" << endl;
		cout << "Name: Rayquaza " << endl;
		cout << "Title: Sky High Pokemon" << endl;
		cout << "Type: Dragon/Flying" << endl;
		cout << "Height: 23'00" << endl;
		cout << "Weight: 455.2lbs" << endl;
		break;
	default:
		cout << "Please choose a valid option";
	}
}

Pokedex::Pokedex() {
	 id = 0;
	 name = "";
	 title = "";
	 height = 0;
	 weight = 0;
};

void Pokedex::printStats(int id, string name, string title, string type, int height, int weight) {
	cout << "_____________POKEDEX__________" << endl;
	cout << "No. " << id << endl;
	cout << "Pokemon: " << name << endl;
	cout << title << "Pokemon" << endl;
	cout << "Height: " << height << endl;
	cout << "Weight: " << weight << endl;
}



